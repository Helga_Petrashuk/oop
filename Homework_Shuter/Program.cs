﻿using System;


class Program
{
    static void Main(string[] args)
    {
        ConsoleKeyInfo cki;
        Player player = new Player();
        do
        {
            cki = Console.ReadKey();
            Console.WriteLine();
            Pistol pistol = new Pistol();
            MashineGun gun = new MashineGun();
            
            switch (cki.Key)
            {
                case ConsoleKey.D1:
                    Console.WriteLine("Press 'key 1' - Choose pistol");
                    player.SetWeapon(pistol);
                    break; 
                case ConsoleKey.D2:
                    Console.WriteLine("Press 'key 2' - Choose mashinegun");
                    player.SetWeapon(gun);
                    break;
                case ConsoleKey.D0:
                    Console.WriteLine("Press 'key 0' - Player whit out pistol");
                    player.SetWeapon(null);
                    break;
                case ConsoleKey.Spacebar:
                    Console.WriteLine("Key 'spasebar' - Player shoot");
                    player.UseWeapon();
                    break;
                default:
                    break;
            }
        }
        while (cki.Key != ConsoleKey.Escape);
      
        Console.WriteLine("f Key esc - close programm");

    }
}
