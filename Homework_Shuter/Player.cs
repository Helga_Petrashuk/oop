﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

class Player
{
    protected Weapon weapon;
    private bool option;
    private bool logic = false;
    public void SetWeapon(Weapon weapon)
    {
        this.weapon = weapon;
        if (weapon is Pistol)
        {
            Console.WriteLine("This is pistol");
        }
    }

    public void UseWeapon()
    {
        if (weapon != null)
        {
            weapon.Shoot();
        }
        else
        {
            Console.WriteLine("Weapon is not exist");
        }
    }
}