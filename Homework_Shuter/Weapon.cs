﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

public abstract class Weapon : IDisplayable
{
    protected string name;
    protected static int count = 1;
    public abstract void Display();
    public abstract void Shoot();
}

public interface IDisplayable 
{
    void Display();
}